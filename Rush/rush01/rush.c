#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void	write_array(int versus[], int size);

int	main(int param, char  **argv)
{
	int	i = 1;
	int	eje[25];
	int  versus[50];
	int	 lados;
	int flat;

	flat = 0;
	lados = (param -1)/4;

	while (i < param)
	{
		eje[i] = atoi(argv[i]); 
		i++;
	}

	i = 1;
	while (i < param)
	{
		if (eje[i] > 0 && eje[i] <= lados)
		{
			if (eje[i] == lados  && eje[lados + i] == 1 && (i < 5)) 
			{
				flat++;
				versus[i] = 1;
				versus[lados + i] = 4;
			}
			else if (eje[i] == lados && eje[lados + i] == 1 && (i > 8)) 
			{
				flat++;
				versus[i] = 1;
				versus[lados + i] = 4;
			}
			else if (eje[i] == (lados - 1) &&  eje[lados + i] == (lados - 2) && (i < 5)) 
			{
				flat++;
				versus[i] = 1;
				versus[lados + i] = 2;
			}
			else if (eje[i] == (lados - 1) &&  eje[lados + i] == (lados - 2) && (i > 8)) 
			{
				flat++;
				versus[i] = 2;
				versus[lados + i] = 1;
			}
			else if (eje[i] == (lados - 2) &&  eje[lados + i] == (lados - 2) && (i < 5)) 
			{
				flat++;
				versus[i] = 3;
				versus[lados + i] = 2;
			}
			else if (eje[i] == (lados - 2) &&  eje[lados + i] == (lados - 2) && (i > 8)) 
			{
				flat++;
				versus[i] = 3;
				versus[lados + i] = 2;
			}
			else if (eje[i] == (lados - 3) &&  eje[lados + i] == (lados - 2) && (i < 5)) 
			{
				flat++;
				versus[i] = 4;
				versus[lados + i] = 3;
			}
			else if (eje[i] == (lados - 3) &&  eje[lados + i] == (lados - 2) && (i > 8)) 
			{
				flat++;
				versus[i] = 4;
				versus[lados + i] = 3;
			}
		i++;	

		}
		else
		{
			printf(" Error.  Dato fuera de rango. \n");
				 break;
		}
	}
	
	i = 1;
	while (i < param)
	{
		if(versus[i] <= 0 && flat < 8)
		{
			printf("\n  Error. No tiene solución\n");
				 break;
		}
		else if (flat >= 8)
		{
			write_array(versus, param);
			printf("\nTIENE SOLUCION \n ");
				break;
		}
		i++;
	}
	return (0);
}

void	write_array(int *versus, int size)
{
	int i;
	int j;
	int *swap;
	int	box[4][4];

	swap = &versus[0];
	i = 0;
	j = 0;
	while (i < size)
	{
		box[j][i % 4] = swap[i + 1];
		i++;
		if(i % 4 == 0)
		{
			j++;
		}
	}

	i = 0;
	while (i < size/4)
	{
		j = 0;
		while (j < size/4)
		{
			printf(" [%d] ", box[i][j]);
			j++;
		}
		i++;
		//write(1, "\n", 1);
		printf("\n");
	}
}
