/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush04.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/05 12:05:41 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/05 15:26:24 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
void	ft_putchar(char letter);
void	first(int x);
void	middle(int x);
void	last(int x);

void	rush(int x, int y)
{
	int	j;

	j = 1;
	if (x > 0 && y > 0)
	{
		while (j <= y)
		{
			if (j == 1)
			{	
				first(x);
			}
			else if (j == y)
			{
				last(x);
			}
			else
			{
				middle(x);
			}
		j++;
		}
	}
}

void	first(int x)
{
	char	i;
	char	letter;

	i = 1;
	while (i <= x)
	{
		if (i == 1)
		{
			letter = 'A';
			ft_putchar(letter);
		}
		else if (i == x)
		{
			letter = 'C';
			ft_putchar(letter);
		}
		else
		{
			letter = 'B';
			ft_putchar(letter);
		}
		i++;
	}
	ft_putchar('\n');
}

void	middle(int x)
{
	char	i;
	char	letter;

	i = 1;
	while (i <= x)
	{
		if (i == 1)
		{
			letter = 'B';
			ft_putchar(letter);
		}
		else if (i == x)
		{
			letter = 'B';
			ft_putchar(letter);
		}
		else
		{
			letter = ' ';
			ft_putchar(letter);
		}
	i++;
	}
	ft_putchar('\n');
}

void	last(int x)
{
	char	i;
	char	letter;

	i = 1;
	while (i <= x)
	{
		if (i == 1)
		{
			letter = 'C';
			ft_putchar(letter);
		}
		else if (i == x)
		{
			letter = 'A';
			ft_putchar(letter);
		}
		else
		{
			letter = 'B';
			ft_putchar(letter);
		}
		i++;
	}
	ft_putchar('\n');
}
