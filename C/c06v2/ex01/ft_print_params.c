/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/19 17:01:23 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/20 08:31:53 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char *str)
{
	int	i;

	i = 0;
	while (str[i] != '\0')
	{
		write(1, &str[i], 1);
		i++;
	}
}

int	main(int param, char **argv)
{
	int	i;

	i = 1;
	while (i < param)
	{
		ft_putchar(argv[i]);
		write(1, "\n", 1);
		++i;
	}
	return (0);
}
