/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_int_tab.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/14 17:46:01 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/14 18:04:46 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_rev_int_tab(int *tab, int size)
{
	int	i;
	int	a;

	i = 0;
	while (i < size / 2)
	{
		a = tab[i];
		tab[i] = tab[size - i - 1];
		tab[size - i - 1] = a;
		i++;
	}	
}
/*
#include <unistd.h>
#include <stdio.h>
int	main(void)
{
	int array[6] = {0, 1, 2, 3, 4, 5};
	ft_rev_int_tab(array, 6);
	int i = 0;
	while (i < 6)
	{
		printf("%d", array[i]);
		i++;
	}
	return 0;
}
*/
