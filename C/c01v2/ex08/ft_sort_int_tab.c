/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/09 13:15:24 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/14 17:44:22 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_sort_int_tab(int *tab, int size)
{	
	int	index_i;
	int	index_j;
	int	pivot;

	index_i = 0;
	index_j = 0;
	while (index_i < size - 1)
	{
		while (index_j < size - index_i -1)
		{
			if (tab[index_j] > tab[index_j + 1])
			{
				pivot = tab[index_j + 1];
				tab[index_j + 1] = tab[index_j];
				tab[index_j] = pivot;
			}
			index_j++;
		}
		index_j = 0;
		index_i++;
	}
}
/*
#include <stdio.h>
int	main()
{
	int	array[5] = {5, 2, 8, 9, 3};
	int s = 5;
	int i = 0;

	ft_sort_int_tab(array, s);
	while (i < s)
	{
		printf("%i\n", array[i]);
		i++;
	}
	return 0;
}
*/
