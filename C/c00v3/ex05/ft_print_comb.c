/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/07 09:53:00 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/07 11:11:55 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>

void	ft_print_comb(void)
{
	int	pos_a;
	int	pos_b;
	int	pos_c;

	pos_a = 50;
	pos_b = 49;
	pos_c = 48;
	while (pos_c <= 55)
	{
		while (pos_b <= 56)
		{
			while (pos_a <= 57)
			{
				write(1, &pos_c, 1);
				write(1, &pos_b, 1);
				write(1, &pos_a, 1);
				if (pos_c != 55)
					write(1, ", ", 2);
				pos_a++;
			}
		pos_a = ++pos_b + 1;
		}
	pos_b = ++pos_c;
	}
}
