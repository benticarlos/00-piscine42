/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_combn.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/05 16:24:11 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/08 13:17:19 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>

void	print_numbers(int value, int in, int elevate_number)
{
	char	v;

	if (in < elevate_number)
	{
		write(1, "0", 1);
	}
	while (value != 0)
	{
		v = (value % 10) + 48;
		write(1, &v, 1);
		value /= 10;
	}
}

int	elevado(int n, int elevado)
{
	int	value;
	int	index;

	value = 1;
	if (n != 0)
	{
		index = 1;
		while (index <= elevado)
		{
			value *= n;
			index++;
		}
	}
	else
	{
		return (0);
	}
	return (value);
}

int	print(int status, int in, int elevate_number)
{
	int	inverse_number;
	int	last_value;
	int	value;
	int	n;

	inverse_number = 0;
	last_value = 10;
	n = in;
	while (n != 0)
	{
		inverse_number *= 10;
		value = (n % 10);
		inverse_number += value;
		n /= 10;
		if (value >= last_value)
		{
			return (-1);
		}
		last_value = value;
	}
	print_numbers(inverse_number, in, elevate_number);
	return (status);
}

void	function(int n)
{
	int	in;
	int	elevate_number;
	int	max_size;
	int	verificar;

	in = 12345678 / elevado(10, 9 - n);
	elevate_number = elevado(10, n - 1);
	max_size = elevate_number * 9;
	while (in <= max_size)
	{
		if (print(1, in, elevate_number) == 1)
		{
			verificar = in / elevate_number;
			if (verificar == (10 - n))
			{
				in = max_size + 1;
			}
			else
			{
				write(1, ", ", 2);
			}
		}
		in++;
	}
}

void	ft_print_combn(int n)
{
	if ((n <= 9) && (n >= 1))
	{
		function(n);
	}
}
