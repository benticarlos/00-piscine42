/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/03 13:10:42 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/05 23:34:12 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>

int	power(int houses)
{
	int	answer;

	answer = 1;
	while (houses > 0)
	{
		answer = answer * 10;
		houses--;
	}
	return (answer);
}

int	how_many_houses(int houses, int nb)
{
	while ((nb / 10) >= 1)
	{
		nb = nb / 10;
		houses++;
	}
	return (houses);
}

int	negative(int nb)
{
	if (nb < 0)
	{
		write(1, "-", 1);
		if (nb == -2147483648)
		{
			write(1, "2", 1);
			nb = 147483648;
		}
		else
		{
			nb = -1 * nb;
		}
	}
	return (nb);
}

void	ft_putnbr(int nb)
{
	char	ca;
	char	houses;

	nb = negative(nb);
	houses = how_many_houses(0, nb);
	while (houses > 0)
	{
		ca = (nb / power(houses));
		nb = nb - (ca * power(houses));
		ca = ca + 48;
		write(1, &ca, 1);
		ca = 0;
		houses--;
	}
	ca = (nb + 48);
	write(1, &ca, 1);
}
