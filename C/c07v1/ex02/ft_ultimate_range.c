/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_range.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/20 19:05:40 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/22 09:56:58 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>

int	ft_ultimate_range(int **range, int min, int max)
{
	int	i;

	if (range == NULL)
		return (-1);
	if (max <= min)
		return (0);
	*range = malloc(sizeof(int) * (max - min));
	if (*range == NULL)
		return (-1);
	i = 0;
	while (i < max - min)
	{
		(*range)[i] = min + i;
		i++;
	}
	return (i);
}
/*
int	main(void)
{
	int mi = 5;
	int ma = 10;
	int	*test;
	int	size;
//	int	array[10] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
//	ft_ultimate_range(*array, mi, ma);
	size = ft_ultimate_range(&test, mi, ma);
	printf("size = %d\n", size);
	return 0;
}
*/
