/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/20 20:36:29 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/22 10:23:53 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>

char	*free_string(void)
{
	char	*str;

	str = malloc(1);
	*str = 0;
	return (str);
}

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strcat(char *dest, char *src)
{
	int	n;
	int	i;

	n = ft_strlen(dest);
	i = 0;
	while (src[i])
	{
		dest[n + i] = src[i];
		i++;
	}
	dest[n + i] = 0;
	return (dest);
}

char	*ft_strjoin(int size, char **strs, char *sep)
{
	char	*result;
	int		i;
	int		res_size;

	if (size == 0)
		return (free_string());
	res_size = ft_strlen(sep) * (size - 1);
	i = 0;
	while (i < size)
		res_size += ft_strlen(strs[i++]);
	result = malloc(res_size + 1);
	if (result == NULL)
		return (NULL);
	result[0] = '\0';
	i = 0;
	while (i < size - 1)
	{
		ft_strcat(result, strs[i]);
		ft_strcat(result, sep);
		i++;
	}
	ft_strcat(result, strs[i]);
	return (result);
}
/*
#include <stdio.h>
int	main(void)
{
	int	len_strs = 3;
	char	*cads[3];
	char	*sepa;
	

	cads[0] = "Hola mundo";
	cads[1] = " a todos";
	cads[2] = " desde la 42madrid.";
	cads[3] = " Y adios";
	ft_strjoin(len_strs, **cads, *sepa);
	return 0;
}
*/
