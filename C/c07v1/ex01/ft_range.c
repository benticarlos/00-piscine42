/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/20 16:28:05 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/22 09:52:41 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>
#include <stdio.h>

int	*ft_range(int min, int max)
{
	int	*range;
	int	i;

	if (max <= min)
		return (0);
	range = malloc(sizeof(int) * (max - min));
	i = 0;
	while (min < max)
	{
		range[i] = min;
		i++;
		min++;
	}
	return (range);
}
/*
int	main(void)
{
	int ma;
	int mi;
	int	i = 0;
	int array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

	mi = -5;
	ma = 5;
	while (i < ma)
	{
		array[i] = *ft_range(mi, ma);
		i++;
	}
	printf("range max: %d\n", array[ma]);
	printf("range min: %d\n", array[mi]);
	return 0;
}
*/
