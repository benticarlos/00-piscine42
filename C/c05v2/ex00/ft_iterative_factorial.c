/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 16:27:47 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/19 11:33:08 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int	i;

	i = nb;
	if (i == 0 || i == 1)
		return (1);
	else if (i < 0)
		return (0);
	while (i != 1)
	{
		--i;
		nb = nb * i;
	}
	return (nb);
}
/*
#include <stdio.h>
int main(void)
{
	int number = 8;
	printf(" %i! = %d\n", number, ft_iterative_factorial(number));
	return 0;
}
*/
