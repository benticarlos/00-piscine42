/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/19 11:34:57 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/19 11:40:05 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_power(int nb, int power)
{
	int	i;
	int	num;

	if (power < 0)
		return (0);
	else if (power == 0)
		return (1);
	else
	{
		i = 1;
		num = nb;
		while (i < power)
		{
			num *= nb;
			i++;
		}
		return (num);
	}
}
/*
#include <stdio.h>
int main(void)
{
	int number = 2;
	int pow = 8;
	printf(" %i^%i = %d\n", number, pow,  ft_iterative_power(number, pow));
	return 0;
}
*/
