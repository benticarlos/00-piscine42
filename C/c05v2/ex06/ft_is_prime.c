/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_prime.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/19 13:02:41 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/19 13:48:42 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_is_prime(int nb)
{
	int	i;

	i = 2;
	while (i <= nb)
	{
		if (nb % i == 0)
		{
			if (i == nb)
				return (1);
			else
				return (0);
		}
		i++;
	}
	return (0);
}
/*
#include <stdio.h>
int main(void)
{
	int number = 4;
	printf(" %i is prime? = %d\n", number, ft_is_prime(number));
	return 0;
}
*/
