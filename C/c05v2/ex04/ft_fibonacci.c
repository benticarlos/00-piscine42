/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_fibonacci.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/19 11:48:04 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/19 15:55:44 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_fibonacci(int index)
{
	if (index < 0)
		return (-1);
	if (index == 0)
		return (0);
	if (index == 1)
		return (1);
	return (ft_fibonacci(index - 1) + ft_fibonacci(index - 2));
}
/*
#include <stdio.h>
int main(void)
{
	//int number = -42; // return: -1
	//int number = 0; // return: 0
	//int number = 1; // return: 1
	int number = 2; // return: 1
	printf(" %i fibo = %d\n", number, ft_fibonacci(number));
	return 0;
}
*/
