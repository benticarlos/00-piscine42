/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 09:09:19 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/18 15:54:17 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	digit(char s, char *base)
{
	int	n;

	n = 0;
	while (base[n] != 0 && base[n] != s)
		n++;
	return (n);
}

int	to_verify(char *base)
{
	int	i;
	int	o;

	i = 0;
	o = 0;
	while (base[i])
	{
		o = 0;
		while (o < i)
		{
			if (base[i] == base[o] || (base[i] >= 9 && base[i] <= 13)
				|| base[i] == 32 || base[i] == '-' || base[i] == '+')
				return (0);
			o++;
		}
		i++;
	}
	if (i <= 1)
		return (0);
	return (i);
}

int	ft_atoi_base(char *str, char *base)
{
	int	basesize;
	int	nb;
	int	neg;

	nb = 0;
	neg = 1;
	basesize = to_verify(base);
	if (basesize <= 1)
		return (0);
	while (*str != 0 && ((*str >= 9 && *str <= 13) || *str == 32))
		str++;
	while (*str != 0 && (*str == '-' || *str == '+'))
	{
		if (*str++ == '-')
			neg *= -1;
	}
	while (*str != 0 && (digit(*str, base) < basesize))
	{
		nb = (basesize * nb) + (digit(*str, base));
		str++;
	}
	return (nb * neg);
}
/*
#include <stdio.h>
int main()
{
	int	n;
//	n = ft_atoi_base("256", "01234567");//-12066
	n = ft_atoi_base("100", "01234567"); // 64 octal
//	n = ft_atoi_base("100000000", "01"); // 256 binary
//	n = ft_atoi_base("", "01");
	printf("%d\n", n);
}
*/
