/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 09:03:08 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/18 19:00:38 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	put_char(char c)
{
	write(1, &c, 1);
}

void	ft_putnbr(int nb)
{
	if (nb < 0)
	{
		nb = -nb;
		put_char('-');
	}
	if (nb == -2147483648)
	{
		put_char('2');
		nb = 147483648;
	}
	if (nb < 10)
	{
		put_char(nb + '0');
	}
	else if (nb >= 10 && nb <= 2147483647)
	{
		ft_putnbr (nb / 10);
		ft_putnbr (nb % 10);
	}	
}
/*
int main(void)
{
	ft_putnbr(42);
	write(1, "\n", 1);
	ft_putnbr(-42);
	write(1, "\n", 1);
	ft_putnbr(-2147483648);
	write(1, "\n", 1);
	ft_putnbr(147483648);
	write(1, "\n", 1);
	return 0;
}
*/
