/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 09:04:26 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/18 16:00:39 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(char *str)
{
	int	i;
	int	n;
	int	result;

	i = 0;
	n = 1 ;
	result = 0;
	while (str[i] == 32 || (str[i] >= 9 && str[i] <= 13))
		i++;
	while (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			n *= -1;
		i++;
	}
	while (str[i] != '\0' && str[i] >= '0' && str[i] <= '9')
	{
		result = result * 10;
		result = result + str[i] - '0';
		i++;
	}
	return (result * n);
}
/*
#include <stdlib.h>
#include <stdio.h>
int	main(int param, char **argv)
{
	int	i;
	int	result;
	int	ft_result;
	i = 1;
	while (i < param)
	{
		printf("to convert[%i]: %s\n", i, argv[i]);
		ft_result = ft_atoi(argv[i]);
		result = atoi(argv[i]);
		printf("ft_result: %d\n", ft_result);
		printf("result: %d\n", result);
		++i;
	}
}
*/
