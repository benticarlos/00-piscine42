/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 09:48:01 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/15 17:55:51 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcapitalize(char *str)
{
	int	i;
	int	character;

	i = 0;
	while (str[i] != '\0')
	{
		if ('a' <= str[i -1] && str[i - 1] <= 'z')
			character = 1;
		else if ('0' <= str[i -1] && str[i - 1] <= '9')
			character = 1;
		else if ('A' <= str[i -1] && str[i - 1] <= 'Z')
			character = 1;
		else
			character = 0;
		if ('A' <= str[i] && str[i] <= 'Z' && character == 1)
		{
			str[i] = str[i] + 32;
		}
		if ('a' <= str[i] && str[i] <= 'z' && character == 0)
		{
			str[i] = str[i] - 32;
		}
		i++;
	}
	return (str);
}
/*
#include <unistd.h>
#include <stdio.h>
int	main(void)
{
	char	s[] = "salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un";
	ft_strcapitalize(s);
	printf("%s\n", s);
	return 0;
}
*/
