/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_alpha.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 08:41:29 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/15 16:42:47 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_str_is_alpha(char *str)
{
	int	i;
	int	temp;

	temp = 1;
	i = 0;
	while (*str)
	{
		if (!(str[i] >= 97 && str[i] <= 122) && !(str[i] >= 65 && str[i] <= 90))
		{
			temp = 0;
		}
		str++;
	}
	return (temp);
}
/*
#include <unistd.h>
#include <stdio.h>
int	main()
{
	int r;
	char	str[20] = "abz";
	r = ft_str_is_alpha(str);
	printf("%d", r);
	return 0;
}
*/
