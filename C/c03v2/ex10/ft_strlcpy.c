/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/16 08:14:23 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/16 22:31:12 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strlen(char *str);

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size)
{
	unsigned int	i;
	int				src_length;

	i = 0;
	src_length = ft_strlen(src);
	if (size > 0)
	{
		size--;
		while (src[i] != '\0' && i < size)
		{
			dest[i] = src[i];
			i++;
		}
		dest[i] = '\0';
	}
	return (src_length);
}

int	ft_strlen(char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}
/*
#include <stdio.h>
#include <string.h>
int	main(void)
{
	char	source[30] = {"Cadena Fuente!"};
	char	destination[255] = {"Cadena Destino,  mas larga "};

	printf("FT: Destination:. \"%s\" \n", destination);
	printf("FT: Result= %d", ft_strlcpy(destination, source, 28));
	printf("Destination:. \"%s\" \n", destination);
	printf("Result= %d", ft_strlcpy(destination, source, 28));
	printf("\nString= %s\n", destination);
	printf("Result= %li", strlcpy(destination, " MAS CADENA! ", 15));
	printf("\nString= %s\n", destination);
	printf("Result= %li", strlcpy(destination, " Nueva entrada ", 4));
	printf("\nString= %s\n", destination);
	return (0);	
}
*/
