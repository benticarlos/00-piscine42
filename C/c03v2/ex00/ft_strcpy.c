/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/11 09:20:03 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/15 16:20:12 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcpy(char *dest, char *src)
{
	int	i;

	i = 0;
	while (src[i] != '\0')
	{
		dest[i] = src [i];
		i++;
	}
	dest[i] = '\0';
	return (dest);
}
/*
#include <unistd.h>
#include <stdio.h>
#include <string.h>
int	main()
{
	char	dest[50] = "Mundo";
	char	src[50] = "Hola";
	ft_strcpy(dest, src);
	//strcpy(dest, src);
	printf("%s\n", src);
	return (0);
}
*/
