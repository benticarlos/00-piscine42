/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 09:23:19 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/15 17:34:30 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_str_is_printable(char *str)
{
	int	i;
	int	temp;

	temp = 1;
	i = 0;
	while (*str)
	{
		if (!(str[i] >= 32 && str[i] <= 127))
		{
			temp = 0;
		}
		str++;
	}
	return (temp);
}
/*
#include <unistd.h>
#include <stdio.h>
int	main(void)
{
	int result;
	char	str[] = "c";
	result = ft_str_is_printable(str);
	printf("%d", result);
	return 0;
}
*/
