/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_uppercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 09:16:17 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/15 16:57:26 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_str_is_uppercase(char *str)
{
	int	i;
	int	temp;

	temp = 1;
	i = 0;
	while (*str)
	{
		if (!(str[i] >= 65 && str[i] <= 90))
		{
			temp = 0;
		}
		str++;
	}
	return (temp);
}
/*
#include <unistd.h>
#include <stdio.h>
int	main(void)
{
	int result;
	char	str[] = "AMA!Z";
	result = ft_str_is_uppercase(str);
	printf("%d", result);
	return 0;
}
*/
