/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid.com>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/12 09:42:59 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/15 16:23:43 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	unsigned int	i;

	i = 0;
	while (src[i] != '\0' && i < n)
	{
		dest[i] = src[i];
		i++;
	}
	while (i < n)
	{
		dest[i] = '\0';
		i++;
	}
	return (dest);
}
/*
#include <stdio.h>
#include <unistd.h>
#include <string.h>
int	main(void)
{
	char str1[] = "scripting";
	char *str2 = "maDrid";
	printf("ft_strncpy: %s\n", ft_strncpy(str1, str2, 5));
	printf("strncpy: %s\n", strncpy(str1, str2, 5));
	return 0;
}
*/
