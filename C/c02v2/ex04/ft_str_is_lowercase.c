/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_lowercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 09:04:32 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/15 16:49:17 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_str_is_lowercase(char *str)
{
	int	i;
	int	temp;

	i = 0;
	temp = 1;
	while (str[i] != '\0')
	{
		if (!(str[i] >= 97 && str[i] <= 122))
		{
			temp = 0;
		}
		i++;
	}
	return (temp);
}
/*
#include <unistd.h>
#include <stdio.h>
int	main(void)
{
	int result;
	char	str[] = "asdf";
	result = ft_str_is_lowercase(str);
	printf("%d", result);
	return 0;
}
*/
