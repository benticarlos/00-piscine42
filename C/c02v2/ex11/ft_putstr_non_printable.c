/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/16 08:16:21 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/16 22:36:06 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <unistd.h>

int		is_printable(char c);
char	*convert_to_hex(unsigned char c);

void	ft_putstr_non_printable(char *str)
{
	int		i;

	i = 0;
	while (str[i])
	{
		if (is_printable(str[i]) == 1)
		{
			write(1, &str[i], 1);
		}
		else
		{
			write(1, convert_to_hex(str[i]), 3);
		}
		i++;
	}
}

int	is_printable(char c)
{
	int	result;

	result = 1;
	if (c < ' ' || c > '~')
	{
		result = 0;
	}
	return (result);
}

char	*convert_to_hex(unsigned char c)
{
	char	output[3];
	char	*ptr;
	char	*mask;

	mask = "0123456789abcdef";
	ptr = &output[0];
	output[0] = '\\';
	output[1] = mask[(c >> 4) & 15];
	output[2] = mask[c & 15];
	return (ptr);
}
/*
int	main(void)
{
	char	*c;

	c = "Coucou\ntu vas bien ?";
	ft_putstr_non_printable(c);
	return (0);
}
*/
