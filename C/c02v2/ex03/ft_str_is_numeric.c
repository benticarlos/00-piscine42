/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_numeric.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cbenti-r <cbenti-r@student.42madrid>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 09:00:02 by cbenti-r          #+#    #+#             */
/*   Updated: 2023/02/15 16:43:15 by cbenti-r         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_str_is_numeric(char *str)
{
	int	i;
	int	temp;

	temp = 1;
	i = 0;
	while (*str)
	{
		if (!(str[i] >= 48 && str[i] <= 57))
		{
			temp = 0;
		}
		str++;
	}
	return (temp);
}
/*
#include <unistd.h>
#include <stdio.h>
int	main(void)
{
	int result;
	char	str[] = "123";
	result = ft_str_is_numeric(str);
	printf("%d", result);
	return 0;
}
*/
